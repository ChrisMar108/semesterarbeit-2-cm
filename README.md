## Semesterarbeit 2 Christian Marosi ##
Dieses Dokument ist nur für die kurze Erklärung, wie die Struktur aufgebaut ist für die Semesterarbeit für die Ordner und Dokumente sowie weiteren Dateien.

Dokumente: 
- Hier ist die Produktdokumentation abgelegt des WSUS Operating von der Firma allnet 
- Ebenfalls ist das Formular zur Einreichung der Semesterarbeit hinzugefügt zum die Ziele und weitere Abschnitte zu sehen können
- Zusätzlich ist ein Grober Zeitplan von wann bis wann welches Ziel geplant wurde zu erarbeiten.
- Die Sprint Reviews sind hier ebenfalls zu finden.

Ziel 1 - 3:
- Diese Ordner dienen den einzelnen Zielen, mit dem Zweck, die einzelnen Ziele zu zeigen was erarbeitet wurde und was für Endprodukte es beinhalten

Geändert hat sich dass der Ordner "Scripts" entfernt wurde und dessen Daten nun bei "Ziel 3" zu finden ist, weil dort ja das Script erklärt wird sowie Dokumentiert.