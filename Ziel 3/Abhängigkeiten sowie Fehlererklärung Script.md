# Risiken, Abhängigkeiten und Fehlerquellen im Script
## Es bestehen folgende Abhängigkeiten für das Bereinigungs-Script: <br> 
- Die Erstellung der Aufgabe mit PowerShell benötigt bereits Administratoren-Rechte
- Wenn die Aufgabe erstellt wird, sollte drauf geachtet werden, dass es selbst Regelmässig durchgelaufen wird.
- Der Zeitpunkt vom Ausführen der Aufgabe sollte so gewählt sein, dass wenn es möglich ist, so wenige Benutzer wie möglich auf dem System drauf sind
- Der Name de Aufgabe sollte sinnvoll gewählt werden, so dass der nächste Techniker auch versteht was gemeint wird. Beispielsweise macht "WSUS-CleanUp" schon sehr deutlich, welcher Bereich gemeint wird und was die Aufgabe durchführt.
- Es sollte drauf geachtet werden, von welchem Benutzer aus dieser Task ausgeführt wird, weil wir mal Fälle hatten dass unser "Manager" (Domain Admin jeweils) gesperrt war. Deswegen wird auch das im System vorhandener Benutzer "System" genommen weil das Lokal ist
- Der WSUS-Server hat eine statische IP-Adresse was sich nicht ändert. Bei einer Änderung muss bei jedem Kunden in der Group Policy die IP-Adresse neu angepasst werden.

## Folgende Risiken bestehen mit dem Skript <br>
- Falsche PowerShell-Version für das ausführen vom Script. Es muss drauf geachtet werden dass die richtige PowerShell Version installiert ist damit die Cmdlets auch richtig ausgeführt werden können. Als richtige Server Version wird mindestens Windows Server 2012 benötigt, entsprechend wird mindestens PowerShell Version 3.0 gebraucht als System-Anforderung. Meine Firma hat Windows Server 2019 als Server und PowerShell 5.1, somit ist dieses Risiko nicht vorhanden.
- Die Systemberechtigungen mit dem Benutzer "NT AUTHORITY\SYSTEM" können nicht genug Berechtigt sein, damit die geplante Aufgabe als Script erstellt oder durchgeführt wird.
- Verfügbarbeit des WSUS-Servers. Wenn der WSUS-Server nicht erreichbar oder aber auch Probleme hat, kann das Script oder die geplanten Aufgaben nicht durchgeführt werden.

## Folgende Feherquellen sind möglich mit dem Script <br>
- Die Zeitplanung ist ein sehr starkes Risiko. Der Grund ist weil es sein kann dass um 4 Uhr in der Nacht einige Server in der Neustart-Phase sind und deswegen nicht alles durchgeführt werden kann. 
- Die Aufgabe exisitiert bereits und kann Konflikte im Namen auslösen. 
- Der Pfad des Taskes existiert nicht oder die Berechtigungen fehlen dazu, dann kann das Script nicht ausgeführt werden.
- Fehler in den Power-Shell Befehlen, wo dazu führen können bei den Argumenten oder anderes, dass die WSUS-Bereinigung nicht wie gewolt ausgeführt wird. Hierbei können auch Tippfehler oder auch nicht existierende Cmdlets die möglichen Fehlerquellen sein.
