$trigger = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Sunday -At 04:00
$action = New-ScheduledTaskAction `
-Execute C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe `
-Argument "-Command 'Invoke-WsusServerCleanup -DeclineExpiredUpdates -DeclineSupersededUpdates'"
Register-ScheduledTask -TaskName WSUS-Decline -TaskPath "\WSUS\" `
-Action $action -Trigger $trigger -user "NT AUTHORITY\SYSTEM"

$trigger = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Sunday -At 04:30
$action = New-ScheduledTaskAction `
-Execute C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe `
-Argument "-Command 'Invoke-WsusServerCleanup -CleanupObsoleteUpdates -CleanupUnneededContentFiles'"
Register-ScheduledTask -TaskName WSUS-Cleanup -TaskPath "\WSUS\" `
-Action $action -Trigger $trigger -user "NT AUTHORITY\SYSTEM"
