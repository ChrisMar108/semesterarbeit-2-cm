Zeile 1: <br>
`$trigger = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Sunday -At 04:00` <br>
Es wird eine neue geplante Aufgabe ausgelöst (Wegen $trigger), welcher Wöchentlich ausgeführt wird. Hierbei wird festgelegt, dass die Aufgabe immer am Sonntag um 4 Uhr in der Nacht die Aufgabe ausgeführt werden sollte.

Zeile 2: <br>
`$action = New-ScheduledTaskAction ` <br>
Hierbei wird die Aktion der neuen Aufgabe erstellt.

Zeile 3: <br>
`-Execute C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe` <br>
Die Aufgaben-Erstellung wird mit PowerShell durchgeführt aus der `PowerShell.exe` Datei, welcher im System32 gespeichert ist.

Zeile 4: <br>
` -Argument "-Command 'Invoke-WsusServerCleanup -DeclineExpiredUpdates -DeclineSupersededUpdates'" `<br>
Hierbei wird das Argument als Befehl mitgegeben, dass der Bereinigungsprozess auf dem WSUS-Server gestartet werden sollte, bei der abgelöste oder abgelaufene Updates sowie Abgelehnte oder unterdrückte Updates rausreinigen sollte.

Zeile 5: <br>
`Register-ScheduledTask -TaskName WSUS-Decline -TaskPath "\WSUS\" `<br>
Die geplante aufgabe wird Registriert als Aufgabenname "WSUS-Decline" und wird unter \WSUS\ abgelegt (Unterordner von der Aufgaben-Bibliothek, wo WSUS heisst und dort ist die Aufgabe entsprechend)

Zeile 6:<br>
`-Action $action -Trigger $trigger -user "NT AUTHORITY\SYSTEM"`<br>
Die Aktion wird vom Benutzer "NT AUTHORITY\System" aufgelöst/getriggert.

Zeile 8: <br>
`$trigger = New-ScheduledTaskTrigger -Weekly -WeeksInterval 1 -DaysOfWeek Sunday -At 04:30`<br>
Es wird eine zweite geplante Aufgabe erstellt, welcher ebenfalls Wöchentlich ausgeführt wird allerdings mit dem Unterschied dass es statt 4 Uhr um 4:30 Uhr ausgeführt wird.

Zeile 9: <br>
`$action = New-ScheduledTaskAction `<br>
Hierbei wird die Aktion der neuen Aufgabe erstellt.

Zeile 10: <br>
`-Execute C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe `<br>
Die Aufgaben-Erstellung wird mit PowerShell 1.0 durchgeführt aus der PowerShell.exe Datei, welcher im System32 gespeichert ist.

Zeile 11: <br>
`-Argument "-Command 'Invoke-WsusServerCleanup -CleanupObsoleteUpdates -CleanupUnneededContentFiles'"`<br>
Hierbei wird das Argument als Befehl mitgegeben, dass der Bereinigungsprozess auf dem WSUS-Server gestartet werden sollte, bei der überflüssige oder nicht benötigte Updates rausreinigen sollte.

Zeile 12: <br>
`Register-ScheduledTask -TaskName WSUS-Cleanup -TaskPath "\WSUS\"`<br>
Die geplante aufgabe wird Registriert als Aufgabenname "WSUS-Decline" und wird unter \WSUS\ abgelegt (Unterordner von der Aufgaben-Bibliothek, wo WSUS heisst und dort ist die Aufgabe entsprechend)

Zeile 13:<br>
`-Action $action -Trigger $trigger -user "NT AUTHORITY\SYSTEM"`<br>
Die Aktion wird vom Benutzer "NT AUTHORITY\System" aufgelöst/getriggert.

Verwendete Quelle ist Microsofts [New-ScheduledTaskAction](https://learn.microsoft.com/de-ch/powershell/module/scheduledtasks/new-scheduledtaskaction?view=windowsserver2022-ps) und [Register-ScheduledTask](https://learn.microsoft.com/de-ch/powershell/module/scheduledtasks/register-scheduledtask?view=windowsserver2022-ps) verwendet worden um die Einzelnen Befehle mit deren Parameter zu erfahren was gemacht wird und was es bedeutet