# Sprint 2 Review
## Was ist bisher gelungen? <br>
- Die Abhängigkeiten sowie möglichen Fehlererklärungen sind aufgezeigt worden beim Bereinigungsscript. 
- Der Anfang ist gemacht worden für das Kolloquium um entsprechend Präsentation vorbereiten.
- Rückmeldungen hole ich mir mit ChatGPT 4 zuerst, damit ich schon mal Inspiration und um Sicherstellend zu sein für die Dokumentationen und anderes damit ich zeigen kann, wie sehr mein Fortschritt gelungen ist. Es hat für mich aufgezeigt auch wie viel es gibt an Möglichkeiten, wie man sich verbessern kann und wo überall ich mich optimieren und Verbesser kann.

## Was ist noch nicht gelungen?
- Bisher ist mir noch das zweite Ziel noch nicht gelungen vollständig zu erarbeiten, weil in der Firma bisher kein Notfallplan oder WSUS Selbst kein Verfolgungssystem bietet. Dieses Ziel muss ich selbst schauen, wie ich am besten ein Konzept sowie einen Plan erarbeite aber da kommen mir noch die Ideen.
- Die Präsentation für das Kolloquium ist ebenfalls noch erst am Anfang, weil das ein neuer Punkt ist bei der Semesterarbeit und entsprechend in den Zeitplan reinpassen muss.

## Vergleich zu den Projektzielen
- Ziel 1 ist weitgehend erarbeitet worden auf ca. 95% und somit wird im Januar geschaut auf den Feinschliff. Die restlichen 5% sind primär Grammatik, Technisches Verständis und weitere Begriffe zu korrigieren falls es notwendig wird aus eigener Sicht.
- Ziel 2 ist in der Bearbeitung und wird bis Abgabe erledigt sein, hier ist etwa ein 70% Stand bereits, weil das noch weitere Verbesserungen benötigt. Die grössere Herausforderung liegt darin, dass ein effizientes sowie klar Strukturiertes Konzept erstellt werden muss. Hierbei ist die Kreativität der Entscheidende Punkt, was je nach Sache echt eine Schwierigkeit bedeuten kann.
- Ziel 3 mit den möglichen Abhängigkeiten und Fehlerquellen ist erarbeitet worden und lässt aufzeigen, was für mögliche Risiken bestehen können. Die Dokumentation ist zu 95% erarbeitet worden vom Script mit den Abhängigkeiten, Risiken und weiteren Faktoren. 
- Die Präsentation wird ebenfalls noch vorbereitet, damit die Note noch aufgebessert werden kann. Hier ist erst ca. 5% gemacht, das wird im Januar vollständig erarbeitet.
 - Der Zeitplan ist bisher recht gut eingehalten worden und muss nun geschaut werden, wie es noch bis Mitte Januar gut ausgenützt werden kann auch wenn dazwischen noch meine Weihnachtsferien kommen.