# Sprint 1 Review
## Aktueller Stand
Bisher sind mir folgende Dinge gelungen: 
- Das Script, welcher für die WSUS Update Bereinigung benötigt wird, ist so weitgehend dokumentiert mit den einzelnen Befehlen und Parameter wie möglich. 
- Der Ablaufplan, wie der Momentan aussieht ist als Gantt-Diagramm dargestellt worden mit Zeitplan sowie mögliche Risiken aufgeklärt aufgeschrieben. Beim Zeitplan für die Darstellung des Ablaufplans ist eine eigene Variante erstellt worden, wie es besser verteilt werden kann in den Tasks um bessere Übersicht zu haben mit dem Handling

## Vergleich zu den Projektzielen
- Ziel 1 in eigenen Gefühl ist ca. zu vielleicht 80% bereits erarbeitet worden
- Ziel 2 ist noch nicht erarbeitet, ist noch ausstehend bis Sprint 2
- Ziel 3 hat dafür bereits begonnen und ist bei ca. 30% schon bearbeitet worden