# Wie kann dieser Ablaufplan verbessert werden?
## Allgemeine Verbesserungen
Der erste wichtige Punkt schon mal beginnt bei der Anmeldung in die Remotedesktop-Sesstion (RDP-Verbindung). Hierbeit könnten folgende Verbesserungen vorgenommen werden welcher zugleich auch Vorteile bietet:

- Persönlicher Administrator bereits bei der Anmeldung auf den WSUS-Server
- Entsprechend kann man in der Ereignisanzeige verfolgen, wer sich anmeldete und je nach Umständen den Zweck nachfragen
- Beim persönlichen Administrator ist kein Risiko vorhanden, dass man bei der RDS-Session rausgeworfen wird, da man mit einem eigenen Administrator verbunden ist
<br>

Die Primären Optimierungen sind im [Gantt-Diagramm](Gantt-Diagramm%20Aufgabenübersicht.xlsx) zu finden, die erste Tabelle mit Diagramm ist der Momentane Ablauf mit den verschiedenen Tasks, wie die Zeit dort bemessen ist und wie es dargestellt ist. Die zweite Mappe mit "Optimierter Ablauf" zeigt dafür einen Ablauf wie meine eigene Vorstellung eher entspricht, da die Zeit nicht grosszügig genug bemessen wurde und je nach Sachverhalt auch eine Reserve notwendig ist. Der Grund für die grosszügigeren Zeitfenster ist deswegen Gewählt, weil jede Aufgabe unvorsehbare Ereignisse mit sich bringen kann. <br>

## Abgelöste Updates Ablehnen
Hier besteht keine Möglichkeit, Abgelöste Updates automatisiert zu ablehnen bei uns. Die ARM64 Updates konnten leider nicht so eingestellt werden, dass diese Updates automatisch abgelehnt werden. 

## Systeme mit Fehler bereinigen
Das ist im Moment die grösste Baustelle. Es gibt eben Systeme, welche bei Microsoft nicht mehr unterstützt werden wie zum Beispiel Windows 7 bei 2 Geräten oder aber auch 1 Server mit Windows Server 2008 R2, 2012 sowie 2012 R2. Nach und nach werden diese Server dann abgelöst immerhin aber das ist auch schon seit länger geplant allerdings noch immer nicht erfolgreich. Hierbei muss ein Konzept erarbeitet werden, wie am besten die ganzen Ablösungen gemacht werden in einem vernünftigen Budget-Rahmen, aber das muss die Geschäftsleitung mit der Projektleitung gemeinsam schauen. 

## Systeme ohne Status bereinigen
Hier kann wenig ausgerichtet werden mit dem Kunden, falls das System eben nach 30 Tagen keinen Staus gemeldet wird. Hierbei müsste der Kunde schauen, dass der Kunde sein Laptop in der Firma einmal im Monat ist und dort sich mit dem Firmen-Netzwerk verbindet. Ohne das werden keine Updates installiert und wegen das kann die anzahl der Fehlerhaften Systeme in der WSUS-Konsole eben ansteigen. Server, welche seit länger nicht mehr hochgefahren worden sind oder seit mehr als mehr als 25 Tagen kein Status meldeten, werden von Hand angestossen und versucht neu zu melden lassen an den WSUS-Server, damit es wieder eine Statusmeldung abgeben können. Je nach Server allerdings wird mit dem 2nd Level oder Projektleiter abgeklärt, ob der Server aus der WSUS-Konsole entfernt werden darf. Erst wenn der einverstanden ist, darf der Server entfernt werden aus der WSUS-Konsole. 