# Momentaner Ablaufplan
Im Momentanen Ablauf des WSUS-Operatings ist der Ablauf an sich schon mit Risiken verbunden. <br>
Das erste Risiko ist schon bei der Anmeldung via RDP Session zur WSUS Konsole eben denn: <br>

- Es wird ein allgemeiner Domain-Administrator benutzt (Manager)
- Entsprechend Nachteil, wenn jemand ohne Absprache mit demjenigen, wo das WSUS-Operating am durchführen ist, dass der aus der RDS-Session rausgeworfen wird, sobald sich jemand anders auf dem Server sich anmeldet
- Keine Nachverolgung möglich, wer sich wann an den Server angemeldet hat weil es kein persönlicher Administrator ist
- Erst wenn der Manager gesperrt wird im Active Directory, wird er durch 2 andere Administrator Accounts entsperrt (SysTaskUser und X-Monitor)
 - Das Passwort des Domain-Administrator basiert auf ein Schema, bei der das Risiko vorhanden ist, dass wenn das irgendwie nach draussen gelangt, man als Administrator überall reingelangen kann, auf die Kundensysteme ebenfalls

Sobald die RDS-Anmeldung und Anmeldung auf WSUS-Konsole begonnen wurde, fängt der Ablauf mit den folgenden Operating Tasks an: 

- On-Boarding Systeme im WSUS (Wöchentlich)
- Abgelöste Updates ablehnen (Wöchentlich)
- Prüfen und Testen von neu erschienen Updates (Wöchentlich)
- Genehmigung von getesteten Updates (Wöchentlich)
- Systeme mit Fehler bereinigen (Wöchentlich)
- Systeme ohne Status bereinigen (Wöchentlich)
- Abgleich Systeme mit SV Abo (Zweimonatlich)
- Off-Boarding (Bei Bedarf)
- WSUS Serverbereinigung (Wöchentlich, automatisiert)

# Beschreibung der Einzelnen Operating Tasks
## On-Boarding Systeme im WSUS

Sobald neue Systeme beim Kunden oder in der Firma selbst registriert werden während der Aufsetzung vom Gerät, wird entweder per GPO (Group Policy Object) oder Registry File (Weniger erfolgreich als GPO) eingetragen. Sobald das Gerät mit seinem entsprechenden Namen in der WSUS-Konsole erscheint, muss es in die richtige Computer Organisationseinheit geschoben werden. Insgesamt gibt es 4 grosse Gruppen bei uns: 

- Test-Systems (Hier sind interne Test-Systeme von der Firma vorhanden, diese Gruppe bekommt immer als erster die Updates ausgerollt)
- Productive-Systems 1 (Hier sind die meisten Systeme der Kunden vorhanden und werden in den Updates nach den Test-Systemen ausgerollt)
- Productive-Systems 2 (Dies beinhaltet Kunden-Systeme, welche heikler sind und entsprechend auch später Updates bekommen nach Produktiv Systeme 1)
- allnet Cloud (Cloud-Kunden und Cloud-Systeme vorhanden, diese Gruppe bekommt als letzter die Updates verteilt nach Produktiv System 2 Gruppe)


## Abgelöste Updates ablehnen

Es gibt Updates, welche von neu erscheinenden Updates abgelöst werden, wie zum Beispiel Rollup Updates. Abgelöste Updates werden auch im WSUS abgelehnt, wenn die Konsole bei "Anzahl Erforderlich" und "Anzahl Fehlerhaft" als Zahl 0 ausgibt. In erster Linie werden so eben "Microsoft Edge-Beta/Dev Kanalversion Update für ARM64/x86/x64 Editionen" abgelehnt, weil da nicht teilgenommen wird und ARM64/x86 in einem auch weil x86 mit 32-Bit veraltet ist und bei uns keiner mit einem ARM Gerät arbeitet. <br>
Sobald diese Updates abgelehnt sind, wird die Ansicht wieder Aktualisiert um nur noch die Updates in der Übersicht zu haben, welche bereit sind zu den Genehmigungen.

## Prüfen und Testen von neu erschienen Updates

Jedes Update wird vor der Verteilung auf die produktive Systeme bestmöglich geprüft und getestet in diesem Vorgehen: 

1. Auf dem Upstream WSUS-Server werden in der Konsole bei der Updates-Ansicht die Kategorie "Approved for Test-Systems" zuerst geöffnet und nach Freigabe Datum absteigend sortiert, damit die selektive Genehmigung stattfinden kann.
    - Hierbei ist zu beachten dass sobald die nächste Gruppe Updates genehmigt bekommt, diese dann in der Nächsten Kategorie erscheinen dann auf der WSUS-Konsole
2. Bei den Updates, welche erste für eine Computer Gruppe (Also den Test-Systemen) freigegeben sind, werden wie folgt beurteilt: 
    - Für Kundensysteme nicht geeignete oder nicht benötigte Updates kann man direkt ablehnen, dazu muss die WSUS Konsole auch in den Spalten "Anzahl Erfordelich" und "Anzahl Fehlerhaft" die Zahl 0 melden
    - Überprüfung von Internet Communities oder Foren, welche Probleme Melden zu den aktuellen Microsoft Updates und somit diese Updates für eine Zeit zurückgestellt werden müssen
    - Updates, welche seit mindestens einer Woche auf einem Test-System installiert sind und keine Probleme hervorgerufen haben, können beim Operating Task "Genehmigung von getesteten Updates" freigegeben werden

## Genehmigung von getesteten Updates

Jedes Update durchläuft einen Genehmigungszyklus, wobei die von Microsoft herausgegebene Updates automatisch für die Testsysteme freigegeben werden. Wenn ein Update fehlerfrei ist, wird das erst an die nächste Gruppe freigegeben. Die Zeitachse läuft folgendermassen ab: 

- Woche 1 ist Test auf den Testsystemen
- Woche 2 ist Freigabe für Produktiv Systeme 1, was die meisten Kundensysteme sind
- Woche 3 ist dann Freigabe für Produktiv Systeme 2, welche für heicklere Kundensysteme gedacht sind weil dort echt kein Fehlerhaftes Update sein darf
- Woche 4 sind dann immer die Cloud Kunden als letztes an der Reihe

## Systeme mit Fehler bereinigen

Bei der Verteilung und genehmigungen von Updates kann es auch sein, dass Systeme auch Fehler melden. Diese müssen weitgehend behoben werden. Dieser Task beinhaltet den grössten Aufwand von allen, weil Ursache finden, Update herunterladen manuell sowie installieren und weitere Abhängigkeiten eben berücksichtigt werden müssen.

- Auf dem Upstream-Server wird in der WSUS-Konsole die Ansicht "Alle Computer" geöffnet dazu und nach "Fehlerhaft" gefiltert
- Jedes Fehlerhafte System wird manuell untersucht, damit die Fehler auf ein Minimum gehalten wird

Als Vorgehen wird zuerst identifiziert welche Update Installation am scheitern ist per WSUS-Konsole. Im Anschluss wird auf dem betroffenen Server angemeldet und geschaut, welcher Fehlercode angezeigt wird auf dem Server selbst. [Microsoft hat einen Link erstellt, bei der die gängigen Fehlercodes eine Bedeutung haben.](https://learn.microsoft.com/de-ch/troubleshoot/windows-client/deployment/common-windows-update-errors)
Je nach System und Fehlercode allerings gab es auch die Lösung, dass das Update per Microsoft Update Katalog heruntergeladen und manuell rüberkopiert wurde zur Installation.

## Systeme ohne Status bereinigen

Da jedes System eine IPSec VPN Verbindung benötigt um den Status an den WSUS-Server zu übermitteln, kann es insbesonders bei Laptops zu Verzögerungen beim Status an den WSUS-Server zu übermitteln. Die Maximale Zeit der Verzögerung hierbei ist auf 30 Tage begrenzt. Falls es länger keine Rückmeldung gibt muss eingegriffen werden.

- In der WSUS-Konsole wird bei "Alle Computer" nach "Kein Status" gefiltert
    
    - Bei den System die kein Status rappoertieren, müssen korrigiert oder repariert werden
    - Bei Systeme, welche seit mehr als 30 Tagen keinen Status meldeten, müssen vom Kunde zeitnahe mit dem Netzwerk verbunden werden damit es wieder eine Statusmeldung geben können und Updates bekommen

## Abgleich Systeme mit SV Abo

Damit sichergestellt ist, dass auch alle vom Kunden bezahlten Systeme im Service allnet Microsoft Update gemanaged wird, muss wiederkehrend ein Abgleich hierbei stattfinden. Es wird dazu im ERP System die Anzahl der Systeme abgeglichen mit der Anzahl der Systeme in der WSUS-Konsole. Sollte es keine Übereinstimmungen geben, muss mit dem Kundenberater geschaut werden.

## Off-Boarding

Das Wegfallen einen Systems, welcher in der WSUS-Konsole auf dem WSUS Server vorhanden ist, wird gelöscht. Das Wegfallen passiert dadurch, dass das Gerät nicht mehr beim Kunden ist und so entfernt werden kann oder aber abgelöst wurde. Hierbei muss immer mit dem Product Manager geschaut werden, was ein Off-Boarding bekommen darf und so gemeinsam das System zu entfernen auf der WSUS-Konsole. 

## Monitoring und Reporting
Für das Monitoring und somit auch entsprechendes Reporting sind bei uns zwei Tools im Einsatz: Lansweeper, mit der man neben Überwachungen der Systeme auch Reports erstellen kann in der SQL-Datenbank-Sprache sowie auslesen welches Gerät was für ein Betriebssystem hat und wie alt es ist, wann es sich meldete und weitere Informationen. Bei den Servern wird mit ActiveXperts die Kapazität der Festplatten überwacht und entsprechend alarmiert per Mail, wenn es weniger als 10% der Kapazität hat als gelber Alarm, bei unter 5% freiem Speicherplatz mit rotem Alarm.