## Aktuelle Problemstellung
Derzeit gibt es keine Möglichkeit in der Firma, dass das WSUS-Handling irgendwie ein Verfolgungssystem hat. Das Problem ist auch hierbei, dass man nicht sehen kann, welche Updates immer bereit sind zur Freigabe entsprechend immer. Hierbei muss immer wieder geschaut werden auf das Freigabedatum, weil das bestimmt, welche Gruppe das entsprechende Update erhaltet. Wenn das Update zum Beispiel schon 4 Wochen alt ist, kann erst für die Cloud-Kunden und Systeme genehmigt werden, was man immerhin dank der Spalte "Freigabedatum" erkennen kann, wann das entsprechende Update rausgekommen ist. 

## Einrichtung Verfolgungssystem
Um ein System haben zu können hierbei, womit die Windows Updates in der Freigabe verfolgt werden können, macht es am meisten Sinn in erster Linie immer auf das Freigabedazum zu achten und auf die Spalte "Genehmigung" da dass der auschlaggebende Faktor ist von allen. Ebenfalls muss drauf geachtet werden, dass geschaut wird bei der Freigabe was die Zahl beim entsprechenden Update sagt. Wenn es "Freigegeben (1/64)" sagt, bedeutet es dass die Updates erst zu den Test-Systemen freigegeben sind. Anhand davon kann man dann sehen, wo welcher Updates mit der Freigabe steht. Da jedes Update immer selektiv eine Woche eine bestimmte Gruppe durchläuft, kann das Risiko vermindert werden, dass je nach Windows Update das System nachher unbrauchbar wird. 

Diese Grafik zeigt den gesamten Ablauf, wie es mit der Verfolgung aussieht für jedes Einzelne Update, weie der Prozess durchgeht mit der Freigabe und Genehmigung. <br>
![](../Ziel%202/Ablaufdiagramm%20Verfolgungssystem.jpg)

## Anforderungen Verfolgungssystem
Ein ideales Verfolgungssystem hat mehrere Kernpunkte zu erfüllen: 
- Eine benutzerfreundliche Oberfläche (Ist Bereits gegeben mit der WSUS-Konsole)
- Die Nahtlose integration in die IT-Infrastruktur ist bereits vorhanden