## Aktuelle Problematik
Aktuell ist das Problem, dass das WSUS Operating nur von einer Person ausgeführt wird. Ebenfalls ist das Problem hierbei dass wenn diese Person in den Ferien ist oder andere Abwesenheiten hat, dass es mit einer kurzen Übergabe vor und nach der Abwesenheit der entsprechenden Person abgedeckt werden muss.

## Risiken bei diesem Konzept
- Die Person, welche die Übergabe macht zum anderen kann kurzfristig auch ausfallen
- Die andere Person, wo die Übergabe bekommt hat wenig Fachwissen dazu
- Der Person wo das ganze übergeben wird, vergisst das Operating zu erledigen

## Wie konnte es gelöst werden?
Einen oder zwei Tage bevor der Betroffe mit dem Operating in die Ferien geht, wird eine kurze Übergabe gemacht. Es wird entsprechend eine kurze Einführung gemacht, was zu beachten ist von den Aufgaben her und was dabei beachtet werden sollte. Die Übergabe beschäftigt sich in erster Linie nur um die Freigaben der Windows Updates in der WSUS-Konsole, während die Fehlerhaften Systeme aufgrund zu langem Zeitaufwand weggelassen werden. Hierbei entsteht das Risiko, dass die Anzahl der Fehlerhaften Systeme auf eine je nach dem unberechenbare hohe Zahl springen kann. Dieses Risiko kann nicht gemindert werden, weil man ja die Resultate erst nach den gesamten Freigaben und einen Tag später wenn das Betroffene Windows Gerät sich am WSUS wieder meldet, sehen kann ob es fehlerhaft wurde oder ob es weiterhin fehlerfrei bleibt.

## Weitere Optimierungen
Damit die Abhängigkeit nicht nur von einer einzelnen Person getragen wird und in einem aber auch die Risiken gemindert werden sollten, sollte ein Rotationsprinzip eingeführt werden. Hierbei sollte im System-Engineering (SE) Team in regelmässigen Abständen abgewechselt werden. Entsprechend wird nicht nur ein besseres Verständis, sondern auch die Minderung der Risiken erreicht, bei der die Risiken aufgrund der Abwesenheiten oder Ausfälle der Teammitglieder minimiert wird. Entsprechend muss bei diesem Prinzip geplant sowie koordiniert werden anhand eines Rotationskalenders, bei der sichergestellt wird dass jeder drankommt beim WSUS-Operating. 

## Entwicklung des Konzepts sowie Bewertung
Zusätzlich zur Einführung des Rotationsprinzips muss noch eine Schulung gegeben werden, wie das WSUS-Operation funktioniert, damit jedes Teammitglied ist in der Lage den Ablauf zu übernehmen können vom WSUS-Operating. Eine Dokumentation ist bereits vorhanden, wie die Freigabe abläuft und ist auch aktuell.